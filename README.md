## Step to run App

1. setelah clone repo ini, edit file .env berdasarkan database name, user dan password yang ada pada environment local anda
2. pastikan composer dan php sudah terinstal
3. jalankan "composer install"
4. jalankan "php artisan:migrate"
4. jalankan "php artisan db:seed"
5. jalankan "php artisan serve"
6. buka browser dan jalankan app pada "http://localhost:8000/"