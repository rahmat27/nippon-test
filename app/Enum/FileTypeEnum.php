<?php
namespace App\Enum;


enum FileTypeEnum:string
{
    case IMAGE = 'image';
    case DOCUMENT = 'document';
    case VIDEO = 'video';
}