<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelHelper;
use Illuminate\Support\Facades\Auth;

class User extends Model
{
    use HasFactory;
    use ModelHelper;

    protected $table = 'users';

    protected $guarded =  [];

    public $filterable = [
        'username',
        'email',		
	];    

}
