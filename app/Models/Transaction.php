<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelHelper;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{
    use HasFactory;
    use ModelHelper;

    protected $table = 'transaction';

    protected $guarded =  [];

    public function scopegetTransaction($query, $order, $pageSize ){
        
        $data = $query
                ->filter()
                ->with(['warehouse', 'customer'])
                ->orderBy("id", $order)->paginate($pageSize);
       
        return $data;
    }

    public function scopeDetailTransaction($query,$id){
        $data = $query->where('id',$id)->first();
        return $data;
    }

    public function scopeFilter($query)
	{
		$this->filter_model($query, $this);
	}

    public function transaction_detail()
    {
        return $this->hasMany(TransactionDetail::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
