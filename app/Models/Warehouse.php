<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelHelper;

class Warehouse extends Model
{
    use HasFactory;
    use ModelHelper;

    protected $table = 'warehouse';
    protected $guarded =  [];

}
