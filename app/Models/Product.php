<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelHelper;

class Product extends Model
{
    use HasFactory;
    use ModelHelper;

    protected $table = 'product';

    protected $guarded =  [];

}
