<?php

namespace App\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

trait PaginationHelper
{
    public function pagination($data)
    {
        return array(
            "total" => $data->total(),
            "per_page" => $data->perPage(),
            "current_page" => $data->currentPage(),
            "last_page" => $data->lastPage()
        );
    }

    public function paginate($items, $perPage = 5, $page = null)
    {
        $page = $page ? $page : 1;
        $start = ($page - 1) * $perPage;
        $total = sizeof($items);
        $left = ($total % $perPage) > 0 ? 1 : 0;
        $newItems = array_slice($items, $start, $perPage);
        $result=[
            "paginate" => [
                "total" => $total,
                "per_page" => intval($perPage),
                "current_page" => intval($page),
                "last_page" => intval($total / $perPage) + $left,
            ],
            "data" => $newItems,
        ];

        return $result ?? [];
    }
}
