<?php

namespace App\Traits;

use App\Rules\IsUniqueActive;
use App\Rules\IsUniqueOther;
use Illuminate\Support\Facades\Validator;

trait RuleHelper
{
    public function check_input_validation($custom_var = [])
    {
        $controller = get_called_class();
        $method = debug_backtrace()[1]['function'];
        $path = explode('\\', $controller);
        $controller = array_pop($path);
        $datas = request()->all();
        $validator = Validator::make($datas, $this->rules_lists($controller, $method, $custom_var));
        if ($validator->fails()) return $validator->getMessageBag();
        else return null;
    }

   
    private function rules_lists($controller, $method, $params = [])
    {
        //================================LoginController=======================
        if ($controller == "LoginController") {
            if ($method == "login") {
                return [
                    'email' => 'required|max:200',
                    'password' => 'required|min:6',
                ];
            }
        }
        //================================ TransactionController ================================
        elseif ($controller == "TransactionController") {
            if ($method=="store") {
                return [
                    'trx_date' => 'required',
                    'warehouse_id' => 'required|integer',
                    'customer_id' => 'required|integer',
                    'city'  => 'required',
                    'postal_code'  => 'required',
                    'billing_address'  => 'required',
                    'phone_1'  => 'required',
                    'transaction_details' => 'required|array',
                    'transaction_details.*.product_id' => 'required|integer|exists:product,id',
                    'transaction_details.*.quantity' => 'required|integer',
                    'transaction_details.*.unit' => 'required|string',
                    'transaction_details.*.discount' => 'required|int|max:100',

                ];             
            }
        }

    }
}
