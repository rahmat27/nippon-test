<?php

namespace App\Traits;
use Carbon\Carbon;

trait ControllerHelper
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function generate_link($str)
    {
        $str = trim($str);
        return str_replace(" ","-",$str);
    }

    public function upload($path, $photo, $title="")
    {
        $destinationPath = $this->base_upload_dir().$path;
        $photoName = time().(!empty($title)?"_".$title:"").'.'.$photo->getClientOriginalExtension();
        $photo->move($destinationPath, $photoName);
        return $path.$photoName;
    }

    public function upload_log($path, $file, $title="")
    {
        $destinationPath = $this->base_upload_dir().$path;
        $file_name = pathinfo($title, PATHINFO_FILENAME);
        $ext = $this->file_extension($title);
        $name = $file_name.'_'.date("Y-m-d").'.'.$ext;
        $file->move($destinationPath, $name);
        return $path.$name;
    }

    public function file_extension($url){
        $arr = explode(".",$url);
        return strtolower(end($arr));
    }
    
    public function file_type($ext){
        $ext = strtolower($ext);
        $videos = ["mp4","m4a","m4v","f4v","f4a","m4b","m4r","f4b","mov","3gp","3gp2","3g2","3gpp","3gpp2","ogg","oga","ogv","ogx","wmv","wma","asf","webm","flv","avi","hdv","OP1a","OP-Atom","ts","wav","lxf","vob"];
        $images = ['jpg', 'jpeg', 'png'];
        $gifs = ['gif'];
        $texts = ['txt'];
        if (in_array($ext,$images)) return "image";
        if (in_array($ext,$videos)) return "video";
        if (in_array($ext,$texts)) return "text";
        if (in_array($ext,$gifs)) return "gif";
        else return false;
    }

    public function delete_file($path){
        @unlink($this->base_upload_dir.$path);
    }

    private function base_upload_dir(){
        return public_path('/');
    }

    private function asset_dir(){
        return asset('gallery/');
    }

    private function default_file(){
        return public_path('image/not-found.jpg');
    }
    
}