<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {   
        $transactionDetail = [];
        foreach($this->transaction_detail as $detail){
            $transactionDetail[] = [
                'product_name' => $detail->product->name,
                'product_price' => $detail->product->price,
                'quantity' => $detail->quantity,
                'unit' => $detail->unit,
                'discount' => $detail->discount,
            ];
        }
        return [
            'id' => $this->id,
            'trx_code' => $this->trx_code,
            'trx_date' => $this->trx_date,
            'warehouse_name' => $this->warehouse->name ?? null,
            'customer_name' => $this->customer->name?? null,
            'city' => $this->city,
            'billing_address' => $this->billing_address ?? null,
            'postal_code' => $this->postal_code,
            'phone_1' => $this->phone_1,
            'phone_2' => $this->phone_2,
            'total_price' => $this->total_price,
            'description' => $this->description,
            'transaction_detail' => $transactionDetail ?? [],
        ];       
    }
}
