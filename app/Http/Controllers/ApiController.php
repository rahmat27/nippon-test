<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;

class ApiController extends BaseController
{
    public function sendMail($data, $from, $to, $subject, $view)
    {
        $mail = Mail::send($view, $data, function ($m) use ($data, $from, $to, $subject) {
            $m->from($from["email"], $from["name"]);
            $m->to($to["email"], $to["name"])->subject($subject);
        });
        return $mail;
    }

    static function error_responses($result = [], $message = "Failed")
    {
        if (is_string($result)) {
            $message = $result;
            $result = [];
        } else {
            $tmp = $result;
            $message = "Something Trouble";
            if (!empty($tmp)) {
                if (count($tmp) > 0) {
                    foreach ($tmp as $key => $value) {
                        $message = "Error Validation on " . $key . " : " . $value[0];
                        break;
                    }
                }
            }
        }
        return response()->json([
            "status" => false,
            "message" => $message,
            "result" => $result
        ], 404);
    }

    static function error_validation($result = [], $message = "Failed")
    {
        if (is_string($result)) {
            $message = $result;
            $result = [];
        } else {
            $tmp = $result;
            $message = "Something Trouble";
            if (!empty($tmp)) {
                if (count($tmp) > 0) {
                    foreach ($tmp as $key => $value) {
                        $message = "Error Validation on " . $key . " : " . $value[0];
                        break;
                    }
                }
            }
        }
        return response()->json([
            "status" => false,
            "message" => $message,
            "result" => $result
        ], 406);
    }

    /**
     * @param array $result
     * @return \Illuminate\Http\JsonResponse
     */
    static function success_responses($result = [], $message = 'Success')
    {
        return response()->json([
            "status" => true,
            "message" => $message,
            "result" => $result
        ]);
    }

    static function success_paginate_responses($result = [], $paginate =[], $isPaginate=true)
    {
        $response = [
            "status" => true,
            "message" => "Success",
            "result" => $result,
        ];
        
        if ($isPaginate == 'true') {
            $response["paginate"] = $paginate;
        }
        
        return response()->json($response);
    }

    static function success_no_content($result = [], $message = 'Success')
    {
        return response()->json([
            "status" => true,
            "message" => $message,
            "result" => $result
        ],204);
    }

}
