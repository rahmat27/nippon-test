<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Traits\RuleHelper;
use Illuminate\Support\Facades\Hash;

class PasswordForgotController extends ApiController
{
    use RuleHelper;
    public function updatePassword(Request $request)
    {
        $email = Crypt::decrypt($request->email);
        $token = $request->token;
        $user = User::query()->where('email', "=", $email)->first();
        
        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);

        $passwordForgot = PasswordReset::query()->where('email', '=', $email)
            ->where("active", "=", 1)
            ->first();
        if (!$passwordForgot){
            return self::error_responses("Token Credentials Was Exipired");
        }
        $checkHashToken = md5($passwordForgot->token) == $token;
        if (!$checkHashToken){
            return self::error_responses("Token Credentials Is Not Valid");
        }
        $datas["password"] = Hash::make($request->password);
        $update = $user->update($datas);
        if ($update) {
            $passwordForgot = PasswordReset::query()->where('email', '=', $email)
                ->where("active", "=", 1)->update(["active" => 0]);
            return self::success_responses($update);
        } else {
            return self::error_responses("Unkown error");
        }
    }


    public function checkToken(Request $request)
    {   
        try {
            $email = Crypt::decrypt($request->email);
            $token = $request->token;
            $user = User::query()->where('email', "=", $email)->first();
            $getToken = PasswordReset::query()
                ->where('email', '=', $email)
                ->where("active", "=", 1)
                ->first();
            if (!$getToken)
                return self::error_responses("Token Credentials Was Exipired");
            $checkHashToken = md5($getToken->token) == $token;
            if ($checkHashToken)
                return self::success_responses($token);
            else
                return self::error_responses("Token Credentials Is Not Valid");
        }
        catch(DecryptException $e){
            return self::error_responses("Email Is Not Valid");
        }
       
    }

    public function sendLink(Request $request)
    {
        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);
        
        $user = User::query()
            ->whereNull("deleted_at")
            ->where("email", "=", $request->email)
            ->first();
        if ($user) {
            if ($user->active == 0)
                return self::error_responses("You need to activate your account first. Check your email for activation link or contact admin for help");
            
            $response = $user->sendPasswordReset();
            return self::success_responses($response);
        } 
        else {
            return self::error_responses("Send email failed. Email not found.");
        }
    }
}
