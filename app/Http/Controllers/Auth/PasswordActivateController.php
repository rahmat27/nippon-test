<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Models\PasswordActivate;
use App\Models\EmailVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Traits\RuleHelper;
use Illuminate\Support\Facades\Hash;

class PasswordActivateController extends ApiController
{
    use RuleHelper;

    public function updatePassword(Request $request)
    {
        $email = Crypt::decrypt($request->email);
        $token = $request->token;
        $user = User::query()->where('email', "=", $email)->first();

        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);

        $passwordActivation = PasswordActivate::query()->where('email', '=', $email)
            ->where('active', '=', 1)
            ->first();
        if (!$passwordActivation) return self::error_responses("Token Credentials Was Exipired");
        $checkHashToken = md5($passwordActivation->token) == $token;
        if (!$checkHashToken) return self::error_responses("Token Credentials Is Not Valid");
        $user->password = Hash::make($request->password);
        $user->active = 1;
        $update = $user->update();
        if ($update) {
            $passwordActivation = PasswordActivate::query()->where('email', '=', $email)
                ->where('active', '=', 1)->update(["active" => 0]);
            return self::success_responses($user);
        } else {
            return self::error_responses("Unkown error");
        }
    }

    public function checkToken(Request $request)
    {
        try{
            $email = Crypt::decrypt($request->email);
            $token = $request->token;
            $getToken = PasswordActivate::query()
                ->where('email', '=', $email)
                ->where('active', '=', 1)
                ->first();
            if (!$getToken)
                return self::error_responses("Token Credentials Was Exipired");
            $checkHashToken = md5($getToken->token) == $token;
            if ($checkHashToken)
                return self::success_responses($token);
            else
                return self::error_responses("Token Credentials Is Not Valid");
        }
        
        catch(DecryptException $e){
            return self::error_responses("Email Is Not Valid");
        }
    }

    public function sendLink(Request $request)
    {
        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);
        
        $user = User::query()
            ->whereNull("deleted_at")
            // ->where("active", "=", 0)
            ->where("email", "=", $request->email)
            ->first();           
        if ($user) {
            $response = $user->sendPasswordActivation();
            return self::success_responses($response);
        } else {
            return self::error_responses("Send email failed. Email not found.");
        }
    }

    public function updateEmail(Request $request)
    {
        if ($inputError = $this->check_input_validation($request)) {
            return static::error_validation($inputError);
        }
        $email = Crypt::decrypt($request->email);
        $token = $request->token;
        $EmailVerification = EmailVerification::query()->where('email', '=', $email)
            ->where('active', '=', 1)
            ->first();
        if (!$EmailVerification) {
            return self::error_responses("Token Credentials Was Exipired");
        }
        $checkHashToken = md5($EmailVerification->token) == $token;
        if (!$checkHashToken) {
            return self::error_responses("Token Credentials Is Not Valid");
        }
        $user = User::find($EmailVerification->user_id);
        $update = $user->update(["email" => $EmailVerification->email]);
        if ($update) {
            $passwordActivation = EmailVerification::query()->where('email', '=', $email)
                ->where('active', '=', 1)->update(["active" => 0, "expired_at" => date('Y-m-d H:i:s')]);
            return self::success_responses($user);
        } else {
            return self::error_responses("Unkown error");
        }
    }

    public function activate_email(Request $request)
    {
        $email = Crypt::decrypt($request->email);
        $token = $request->token;

        if ($input_error = $this->check_input_validation()) {
            return static::error_validation($input_error);
        }

        $passwordActivation = PasswordActivate::query()->where('email', '=', $email)
            ->where('active', '=', 1)
            ->first();
        if (!$passwordActivation) {
            return self::error_responses("Token Credentials Was Exipired");
        }
        $checkHashToken = md5($passwordActivation->token) == $token;
        if (!$checkHashToken) {
            return self::error_responses("Token Credentials Is Not Valid");
        }
        $user = User::find($passwordActivation->user_id);
        $datas = [];
        $datas["email"] = $passwordActivation->email;
        $datas["active"] = 1;
        $update = $user->update($datas);
        if ($update) {
            $passwordActivation = PasswordActivate::query()->where('email', '=', $email)
                ->where('active', '=', 1)->update(["active" => 0]);
            return self::success_responses($user);
        } else {
            return self::error_responses("Unkown error");
        }
    }
}
