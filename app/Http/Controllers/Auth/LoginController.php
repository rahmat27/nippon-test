<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Traits\RuleHelper;

class LoginController extends ApiController
{
    use RuleHelper;
    
    public function register(Request $request)
    {
        $data = $request->only('name', 'email', 'password', 'username');
        
        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);
        
        $user = User::create([
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return static::success_responses($user, "User Created successfully");
    }

    public function login(Request $request)
    {   
        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);
        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'active' => 1
        );
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                
                return self::error_responses("Login credentials are invalid");
               
            }
        } catch (JWTException $e) {
            // return $credentials;
            return self::error_responses('Could not create token.');
        }
        $user = Auth::user();
        $acces_account = [];
        foreach($user->permissions as $ls){
            array_push($acces_account,$ls['name']);
        }
        $user->access_user = $acces_account;
        $user->role = $user->roles ? $user->roles[0] : null;
        unset($user['roles']);
        $data = [
            'token' => $token,
            'user' => $user,
        ];
        return $this->respondWithToken($data, $token, $acces_account);

    }

    protected function respondWithToken($output, $token, $permissions)
    {
        return static::success_responses($output, "Token is valid")
            ->cookie(env("SET_COOKIE_TOKEN", "access") /* token */, $token, 1440, "/", null, true, false, false, 'None')
            ->cookie(env("SET_COOKIE_PERMISSION", "permissions") /* permission */, json_encode($permissions), 1440, "/", null, true, false, false, 'None');
    }

    public function refresh()
    {   
        try{
            $token = JWTAuth::getToken();
            $user = Auth::user();
            $user->permissions = isset($user->roles[0]->permissions)? $user->roles[0]->permissions : [];
            $acces_account = [];
            foreach($user->permissions as $ls){
                array_push($acces_account,$ls['name']);
            }
            $user->access_user = $acces_account;
            return $this->respondWithToken(JWTAuth::refresh($token), $user);
        }
        catch (JWTException $e){
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                $message = 'Token is Invalid';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){                 
                $message = 'Token is Expired';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 401);
            }else{
                $message ='Authorization Token not found';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 403);
            }
            
        }
    }

    
   

    public function logout(Request $request)
    {        
        try {           
            $token = JWTAuth::getToken();
           
            JWTAuth::invalidate($token, true);

            return static::success_no_content( "User has been logged out");
    
        } catch (JWTException $exception) {
            return static::error_responses("Your token is invalid, user cannot be logged out");
        }
    }

    public function check_token(Request $request)
    {
        try {
            return static::success_responses("Token is valid");
        } catch (JWTException $exception) {
            $valid = "false";
            $message = 'Token is Invalid';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 401);
        }
    }

}
