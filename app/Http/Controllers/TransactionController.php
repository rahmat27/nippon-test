<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\Warehouse;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Traits\RuleHelper;
use App\Traits\PaginationHelper;
use App\Http\Resources\TransactionResource;
Use Exception;
use DB;

class TransactionController extends ApiController
{

    use RuleHelper;
    use PaginationHelper;

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $param = $request->all();
        $pageSize = $request->input('page_size', 10);
        $orderDirection = $request->input('order_direction', "desc");
        $isPaginate = isset($param['is_paginate']) ? $param['is_paginate'] : 'true';
        $data = Transaction::getTransaction($orderDirection, $pageSize, $isPaginate);
       
        return static::success_responses($data);
    }

    public function view()
    {
        return view('transaction-index');
    }

    public function create()
    {
        return view('transaction-create');
    }

    public function store(Request $request)
    {
        try {
            
            DB::beginTransaction();
            if ($inputError = $this->check_input_validation()) {
                return static::error_responses($inputError);
            }

            $transactionData = $request->only(['warehouse_id', 'customer_id', 'trx_date', 'city', 'postal_code', 'billing_address', 'phone_1', 'phone_2', 'description']);
            $trxCode = $this->lastTrxCode();
            
            $transactionData['trx_code'] = $trxCode;
            $transaction = Transaction::create($transactionData);

            $totalPrice = 0;
            foreach ($request->transaction_details as $detail) {
                $product = Product::find($detail['product_id']);
                if (!$product) {
                    throw new \Exception('Product not found');
                }    
                $price = $product->price;
                $quantity = $detail['quantity'];
                $discount = ($detail['discount'] * $price) / 100;
                $totalPrice += ($price * $quantity) - $discount;

                $detail['transaction_id'] = $transaction->id;
                TransactionDetail::create($detail);
            }
            
            $transaction->total_price = $totalPrice;
            $transaction->save();

            DB::commit();

            $transform = TransactionResource::make($transaction);
            return static::success_responses($transform);
           
        } catch (\Exception $e) {
            DB::rollBack();
            return static::error_responses($e->getMessage());
        }
    }

    public function show($id)
    {
        $result = Transaction::DetailTransaction($id);
        if ($result->count() > 0) {
            $transform = TransactionResource::make($result);
            return static::success_responses($transform);
        } else {
            return static::error_responses("No data");
        }
    }

    public function edit($id)
    {
        $transaction = Transaction::with('transaction_detail')->findOrFail($id);
        return view('transaction-show', compact('transaction'));
    }

    public function warehouse()
    {
        $result = Warehouse::get();
        if ($result->count() > 0) {
            return static::success_responses($result);
        } else {
            return static::error_responses("No data");
        }
    }

    public function customer()
    {
        $result = Customer::get();
        if ($result->count() > 0) {
            return static::success_responses($result);
        } else {
            return static::error_responses("No data");
        }
    }

    public function product()
    {
        $result = Product::get();
        if ($result->count() > 0) {
            return static::success_responses($result);
        } else {
            return static::error_responses("No data");
        }
    }
    
    private function lastTrxCode(){
        $lastTransaction = Transaction::orderBy('id', 'desc')->first();
        if ($lastTransaction) {
            $lastTrxCode = $lastTransaction->trx_code;
            $lastTrxNumber = intval(substr($lastTrxCode, 3));
            $newTrxNumber = str_pad($lastTrxNumber + 1, 6, '0', STR_PAD_LEFT);
            $trxCode = 'trx' . $newTrxNumber;
        } else {
            $trxCode = 'trx000001';
        }

        return $trxCode;
    }
 }
