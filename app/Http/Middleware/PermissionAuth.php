<?php

namespace App\Http\Middleware;


use App\Http\Controllers\ApiController;
use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

 
class PermissionAuth  extends ApiController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next, ...$permissions)
    {
        $user = Auth::user();
        $user = User::query()->with('roles', 'roles.permissions')->where("id", "=", $user->id)->first();
        if ($user) {
            // $permissions = $user->roles[0]->permissions;
            foreach ($permissions as $permission) {
                if ($user->hasPermission($permission)) {
                    Log::debug("Auth: Permission Granted");
                    $request->attributes->add(['auth' => (["user"=>$user,"role"=>$permission])]);
                    return $next($request);
                }
            }
        }
        
        Log::debug("Auth: Permission Failed");
        return self::error_responses("Your request not allowed");
    }
}
