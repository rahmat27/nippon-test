<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Crypt;

class UserPasswordActivation extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct($token, $email, $name)
    {
        $this->name = $name;
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        $email = Crypt::encrypt($this->email);
        $token = md5($this->token);
        $url = config('mail.app_email_url')."/set-password?token=".$token."&email=".$email;
        $title = 'Password Activation User';
        return (new MailMessage())
            ->subject($title)
            ->line("Dear ".strtoupper($this->name).",")
            ->line('Welcome to '.config('app.name').'. This is your account activation link, please click button below to set you password and activate your account:')
            ->action('Verify your account', url($url))
            ->line('Your log in email is '.$this->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
