<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;
use URL;

class MailPasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $email,$name)
    {
        $this->name = $name;
        $this->token = $token;
        $this->email = $email;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $address = 'support@inovasiadiwarna.com';
        $subject = 'Password Reset';
        $from = 'SMART COUNT';
        $token = md5($this->token);
        $email = Crypt::encrypt($this->email);
       
        $url = config('mail.app_email_url')."/change-password?email=".$email."&token=".$token;

        return $this->markdown('emails.PasswordReset')
            ->with([
                'name' => $this->name,
                'url' => $url,
                'email' => $email,
            ])
            ->from($address, $from)
            ->replyTo($this->email, strtoupper($this->name))
            ->subject($subject);            
    }
}
