<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;
use URL;

class MailUserVerification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct($token, $email,$name)
    {
        $this->name = $name;
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Get the message envelope.
     */
    public function build()
    {   
        $address = 'support@inovasiadiwarna.com';
        $subject = 'Email Activation';
        $from = 'SMART COUNT';
        $token = md5($this->token);
        $email = Crypt::encrypt($this->email);
        $url = config('mail.app_email_url')."/email-verify?token=".$token."&email=".$email;
        return $this->markdown('emails.EmailVerify')
            ->with([
                'name' => strtoupper($this->name),
                'url' => $url,
                'email' => $this->email,
                'app_name' => config('app.name')
            ])
            ->from($address, $from)
            ->replyTo($this->email, strtoupper($this->name))
            ->subject($subject);        
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            // from: new Address(env('MAIL_FROM_ADDRESS'), 'No Reply'),
            subject: 'Mail Email Verification',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'view.name',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
