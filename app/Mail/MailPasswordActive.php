<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;
use URL;

class MailPasswordActive extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct($token, $email,$name)
    {
        $this->name = $name;
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $address = 'support@inovasiadiwarna.com';
        $subject = 'Password Activation';
        $from = 'SMART COUNT';
        $token = md5($this->token);
        $email = Crypt::encrypt($this->email);
        $url = config('mail.app_email_url')."/activate?email=".$email."&token=".$token;
        

        return $this->markdown('emails.PasswordActive')
            ->with([
                'name' => $this->name,
                'url' => $url,
                'email' => $this->email,
            ])
            ->from($address, $from)
            ->replyTo($this->email, strtoupper($this->name))
            ->subject($subject);        
    }
}
