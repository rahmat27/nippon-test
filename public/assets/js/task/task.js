$(document).ready(function(){
    $("#typeform").change(function(){
        typeform = $(this).val()
        if(typeform == "checkbox" || typeform == "radio"){
            $("#div-option").removeClass('d-none')
        }else{
            $("#div-option").addClass('d-none')
            $("#div-option").html(
                `<input type="hidden" id="nomorinput" value="1">
                  <div class="row">

                    <div class="col-md-4">
                      <input type="text" name="option[]" class="form-control" name="option">
                    </div>
                    <div class="col-md-2 mt-2 div-add">
                      <button type="button" class="btn btn-success btn-sm ml-1 add-input" onclick="addInput()"><i class="fa fa-plus"></i></button>
                    </div>

                  </div>`)

        }
    })
})

function addInput() {
    var nomor = $("#nomorinput").val()
    var nextnomor = parseInt(nomor) + parseInt(1)
    $("#nomorinput").val(nextnomor)
    var addInput = `
    <div class="row mt-3 divinput`+nextnomor+`">

        <div class="col-md-4">
        <input type="text" name="option[]" class="form-control" id="option` + nextnomor + `" name="option">
        </div>
        <div class="col-md-2 mt-2 div-add">
        <button type="button" class="btn btn-danger btn-sm btn-delete-input" id="delete`+nextnomor+`" onclick="deleteInput(`+nextnomor+`)"><i class="fa fa-trash"></i></button>

        </div>

    </div>`

    $("#div-option").append(addInput)
    $(".add-input").remove()
    $("#delete"+nextnomor).after(`<button  type="button" class="btn btn-success btn-sm ml-1 add-input" onclick="addInput()"><i class="fa fa-plus"></i></button>`)
}

function deleteInput(nomor) {
    $(".divinput"+nomor).remove()
    $(".add-input").remove()
    $( ".div-add" ).last().append(`<button type="button" class="btn btn-success btn-sm ml-1 add-input" onclick="addInput()"><i class="fa fa-plus"></i></button>`);

}
