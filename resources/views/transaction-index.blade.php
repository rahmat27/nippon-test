@extends('layouts.app')

@section('title', 'Transaction')

@section('csslib')
<link rel="stylesheet" href="/node_modules/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="/node_modules/clockpicker/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css">
<link rel="stylesheet" href="/node_modules/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="/node_modules/datepicker/css/bootstrap-datepicker.css">
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Transaction</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Transaction</a></div>      
    </div>
  </div>

  <div class="section-body">

    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <button id="btnAdd" type="button" class="btn btn-icon icon-left btn-primary"><i class="fas fa-plus"></i> Create Transaction</button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="dataTable" class="table table-bordered table-md">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>TRX Code</th>
                    <th>Warehouse</th>
                    <th>Customer</th>
                    <th>Total Price</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="tampildata">

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('jslib')
<script src="/node_modules/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js"></script>
@endsection

@section('jsfile')

<script>
  $(document).ready(function() {
      var table = $('#dataTable').DataTable({
          processing: true,
          serverSide: true,
          searching: false,
          ajax: {
              url: 'http://localhost:8000/api/app/transaction/index',
              type: 'GET',
              dataSrc: 'result.data'
          },
          columns: [
              { data: 'id', name: 'id' },
              { data: 'trx_code', name: 'trx_code' },
              { data: 'warehouse.name', name: 'warehouse_name' },
              { data: 'customer.name', name: 'customer_name' },
              { data: 'total_price', name: 'total_price' },
              { data: 'description', name: 'description' },
              { 
                  data: null,
                  orderable: false,
                  searchable: false,
                  render: function(data, type, row) {
                    return '<button class="btn btn-sm btn-primary edit-btn" data-id="' + row.id + '">Show</button>';
                }
              }
          ],
          order: [[0, 'asc']],
      });
  });

  // Handle click event on show buttons
  $('#dataTable').on('click', '.edit-btn', function() {
        var transactionId = $(this).data('id');
        window.location.href = '/edit/' + transactionId;
  });

  $(document).ready(function() {
      $('#btnAdd').click(function() {
        window.location.href = '/create';
      });
  });
  </script>

@endsection