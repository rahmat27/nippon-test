@extends('layouts.app')

@section('title', 'Transaction Detail')

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Transaction Detail</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active">Detail</div>
    </div>
  </div>

  <div class="section-body">

    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h6>Transaction Information</h6>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>TRX Code</th>
                      <td id="trx_code"></td>
                    </tr>
                    <tr>
                      <th>Date</th>
                      <td id="date"></td>
                    </tr>
                    <tr>
                      <th>Total Price</th>
                      <td id="total_price"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-6">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th>Billing Address</th>
                      <td id="billing_address"></td>
                    </tr>
                    <tr>
                      <th>City</th>
                      <td id="city"></td>
                    </tr>
                    <tr>
                      <th>Postal Code</th>
                      <td id="postal_code"></td>
                    </tr>
                    <tr>
                      <th>Phone 1</th>
                      <td id="phone_1"></td>
                    </tr>
                    <tr>
                      <th>Phone 2</th>
                      <td id="phone_2"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <hr>

            <h6>Transaction Details</h6>
            <table class="table table-bordered table-md">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Unit</th>
                  <th>Discount</th>
                </tr>
              </thead>
              <tbody id="transactionDetails">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('jsfile')
<script>
  $(document).ready(function() {
    const transactionId = '{{ request()->route()->parameter('id') }}';

    // Fetch transaction data using AJAX
    $.ajax({
      url: '/api/app/transaction/show/' + transactionId,
      type: 'GET',
      success: function(response) {
        if (response.message === 'Success') {
          const transaction = response.result;
          $('#trx_code').text(transaction.trx_code);
          $('#total_price').text(transaction.total_price); 
          $('#date').text(transaction.trx_date);
          $('#billing_address').text(transaction.billing_address); 
          $('#city').text(transaction.city); 
          $('#postal_code').text(transaction.postal_code); 
          $('#phone_1').text(transaction.phone_1); 
          $('#phone_2').text(transaction.phone_2);

          const transactionDetails = transaction.transaction_detail;
          let tableBody = '';
          let counter = 1;
          transactionDetails.forEach(function(detail) {
            tableBody += `<tr>
              <td>${counter++}</td>
              <td>${detail.product_name}</td>
              <td>${detail.product_price}</td>
              <td>${detail.quantity}</td>
              <td>${detail.unit}</td>
              <td>${detail.discount}</td>
            </tr>`;
          });
          $('#transactionDetails').html(tableBody);
        } else {
          // Handle errors
          console.error('Error fetching transaction data:', response.message);
        }
      },
      error: function(error) {
        console.error('Error:', error);
      }
    });
  });
</script>
@endsection