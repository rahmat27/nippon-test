@extends('layouts.app')

@section('title', 'Create Transaction')


@section('csslib')
<!-- <link rel="stylesheet" href="/node_modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css"> -->
<link rel="stylesheet" href="/node_modules/clockpicker/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css">
<link rel="stylesheet" href="/node_modules/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="/node_modules/datepicker/css/bootstrap-datepicker.css">
@endsection

@section('content')
<section class="section">
  <div class="section-header">
    <h6>Create Transaction</h6>
  </div>

  <div class="section-body">

    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h3>Transaction Information</h3>
          </div>
          <div class="card-body">
            <form id="transactionForm" method="POST" action="{{ route('transaction.store') }}">
              @csrf

              <div class="form-group">
                <label for="trx_date">Transaction Date</label>
                <div class="input-group">
                  <input type="text" class="form-control datepicker" id="trx_date" name="trx_date">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="warehouse_id">Warehouse</label>
                <select class="form-control" id="warehouse_id" name="warehouse_id">
                    <option value="">Select Warehouse</option>
                </select>
            </div>
            
            <div class="form-group">
                <label for="customer_id">Customer</label>
                <select class="form-control" id="customer_id" name="customer_id">
                    <option value="">Select Customer</option>
                </select>
            </div>

              <div class="form-group">
                <label for="billing_address">Billing Address</label>
                <textarea class="form-control" id="billing_address" name="billing_address" rows="3"></textarea>
              </div>

              <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control" id="city" name="city">
              </div>

              <div class="form-group">
                <label for="postal_code">Postal Code</label>
                <input type="text" class="form-control" id="postal_code" name="postal_code">
              </div>

              <div class="form-group">
                <label for="phone_1">Phone 1</label>
                <input type="text" class="form-control" id="phone_1" name="phone_1">
              </div>

              <div class="form-group">
                <label for="phone_2">Phone 2 (Optional)</label>
                <input type="text" class="form-control" id="phone_2" name="phone_2">
              </div>

              <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
              </div>

              <hr>

              <h3>Transaction Details</h3>

              <div id="transactionDetails">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="product_id">Product</label>
                      <select class="form-control" id="product_id" name="transaction_details[0][product_id]" required>
                        <option value="">Select Product</option>
                        </select>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label for="quantity">Quantity</label>
                      <input type="number" class="form-control" id="quantity" name="transaction_details[0][quantity]" min="1" required>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label for="unit">Unit</label>
                      <input type="text" class="form-control" id="unit" name="transaction_details[0][unit]" required>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label for="discount">Discount (Optional)</label>
                      <input type="number" class="form-control" id="discount" name="transaction_details[0][discount]">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button type="button" id="addTransactionDetailBtn" class="btn btn-primary btn-sm">Add More</button>
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-primary">Create Transaction</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('jslib')
<script src="/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/node_modules/datepicker/js/bootstrap-datepicker.js"></script>
@endsection

@section('jsfile')
<script>
  $(function() {
    $('#trx_date').datepicker({
      startDate: new Date(),
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
    });
  });
 
</script>

<script>
  $(document).ready(function() {
    let productOptions = ''; // Store product options globally

    function addTransactionDetail() {
      let transactionDetailCount = $('#transactionDetails').children().length;
      let nextDetailIndex = transactionDetailCount + 1;
      let uniqueProductId = `product_id_${nextDetailIndex}`;

      let newDetail = `
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="${uniqueProductId}">Product</label>
              <select class="form-control product-dropdown" id="${uniqueProductId}" name="transaction_details[${nextDetailIndex}][product_id]" required>
                <option value="">Select Product</option>
                ${productOptions} <!-- Use stored product options -->
              </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="quantity_${nextDetailIndex}">Quantity</label>
              <input type="number" class="form-control" id="quantity_${nextDetailIndex}" name="transaction_details[${nextDetailIndex}][quantity]" min="1" required>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="unit_${nextDetailIndex}">Unit</label>
              <input type="text" class="form-control" id="unit_${nextDetailIndex}" name="transaction_details[${nextDetailIndex}][unit]" required>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="discount_${nextDetailIndex}">Discount (Optional)</label>
              <input type="number" class="form-control" id="discount_${nextDetailIndex}" name="transaction_details[${nextDetailIndex}][discount]">
            </div>
          </div>
          <div class="col-md-2">
            <button type="button" class="btn btn-danger btn-sm" onclick="removeTransactionDetail(this)">Remove</button>
          </div>
        </div>`;

      $('#transactionDetails').append(newDetail);
    }

    window.removeTransactionDetail = function(button) {
        $(button).parent().parent().remove();
    };
    
    $('#addTransactionDetailBtn').on('click', addTransactionDetail);

    function removeTransactionDetail(button) {
      $(button).parent().parent().remove();
    }

    $('#transactionForm').submit(function(e) {
      e.preventDefault();

      const formData = $(this).serialize();

      $.ajax({
        url: '{{ route('transaction.store') }}',
        type: 'POST',
        data: formData,
        success: function(response) {
          if (response.message === 'Success') {
            alert('Transaction created successfully!');
            location.href = '{{ route('transaction') }}';
          } else {
            alert('Error creating transaction: ' + response.message);
          }
        },
        error: function(xhr) {
                if (xhr.status === 404) {
                    let errors = xhr.responseJSON.result;

                    $('.invalid-feedback').remove();
                    $('.is-invalid').removeClass('is-invalid');

                    $.each(errors, function(key, value) {
                        let input = $('[name="' + key + '"]');
                        input.addClass('is-invalid');
                        input.after('<div class="invalid-feedback">' + value[0] + '</div>');
                    });
                } else {
                    alert('An error occurred. Please try again.');
                }
        },
      });
    });


    $.get('/api/app/warehouse', function(response) {
      if (response.status && response.result) {
          warehouseOptions = ''; 
          
          response.result.forEach(function(warehouse) {
              warehouseOptions += '<option value="' + warehouse.id + '">' + warehouse.name + '</option>';
          });

          $('#warehouse_id').append(warehouseOptions);
      
        } else {
          console.error('Failed to fetch warehouses or empty response');
        }
    });

    $.get('/api/app/customer', function(response) {
      if (response.status && response.result) {
          customerOptions = ''; 
          
          response.result.forEach(function(customer) {
              customerOptions += '<option value="' + customer.id + '">' + customer.name + '</option>';
          });

          $('#customer_id').append(customerOptions);
      
        } else {
          console.error('Failed to fetch customers or empty response');
        }
    });
    
    $.get('/api/app/product', function(response) {
      if (response.status && response.result) {
          productOptions = ''; 
          
          response.result.forEach(function(product) {
              productOptions += '<option value="' + product.id + '">' + product.name + '</option>';
          });

          $('#product_id').append(productOptions);
      
        } else {
          console.error('Failed to fetch products or empty response');
        }
    });
   

  });


  

</script>
@endsection
