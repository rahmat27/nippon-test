<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\TransactionController;



Route::group(['prefix' => 'app'], function () {
    Route::group(['prefix' => 'auth'], function ($router) {
        Route::post('/login', [LoginController::class, 'login']);
  
        Route::group(['middleware' => ['jwt.verify']], function () {
            Route::post('/logout', [LoginController::class, 'logout']);
        });
    });
    
    Route::group(['prefix' => 'transaction'], function () {
        Route::get('/index', [TransactionController::class, 'index'])->name('transaction.index');
        Route::post('/store', [TransactionController::class, 'store'])->name('transaction.store');
        Route::get('/show/{id}', [TransactionController::class, 'show'])->name('transaction.show');       
    });

    Route::get('/customer', [TransactionController::class, 'customer'])->name('customer');
    Route::get('/warehouse', [TransactionController::class, 'warehouse'])->name('warehouse');
    Route::get('/product', [TransactionController::class, 'product'])->name('product');

    Route::get('/ping', function () {
        return response()->json([
            "status" => false
        ], 204);
    });

});