<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [TransactionController::class, 'view'])->name('transaction');
Route::get('/edit/{id}', [TransactionController::class, 'edit']);
Route::get('/create', [TransactionController::class, 'create']);
