<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $username = "Admin";
            $email = "Admin@mail.com";
            $password = Hash::make("admin123!!!");

            User::query()->updateOrcreate(["email" => $email], [
                'username' => $username,
                'email' => $email,
                'password' => $password,
            ]);
    }
}
