<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->id();
            $table->string('trx_code');
            $table->dateTime('trx_date');
            $table->unsignedInteger('warehouse_id');
            $table->unsignedInteger('customer_id');
            $table->string('city');
            $table->integer('postal_code');
            $table->text('billing_address');
            $table->string('phone_1');
            $table->string('phone_2');            
            $table->bigInteger('total_price');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaction');
    }
};
