<?php

namespace Database\Factories;

use App\Models\Warehouse;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as FakerFactory;

class WarehouseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Warehouse::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = FakerFactory::create('id_ID'); // Menggunakan locale Indonesia
        $city = $faker->city;

        return [
            'name' => 'Gudang-' . $city,
            'location' => $city,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
