<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as FakerFactory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = FakerFactory::create();
        $productNames = [
            'Bata Merah', 'Semen', 'Pasir', 'Keramik', 'Cat Tembok', 'Pipa PVC', 'Besi Beton',
            'Kaca', 'Kayu', 'Genteng'
        ];

        $productName = $faker->unique()->randomElement($productNames); // Ensure product names are unique
        $price = round($faker->numberBetween(10000, 1000000), -3); // Round price to nearest 1000

        static $number = 1;

        return [
            'product_code' => 'PR' . str_pad($number++, 5, '0', STR_PAD_LEFT),
            'name' => $productName,
            'price' => $price,
            'description' => $faker->sentence,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
