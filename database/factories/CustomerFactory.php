<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as FakerFactory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = FakerFactory::create('id_ID'); // Menggunakan locale Indonesia

        static $number = 1;

        return [
            'customer_code' => 'KS' . str_pad($number++, 5, '0', STR_PAD_LEFT),
            'name' => $faker->name,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
